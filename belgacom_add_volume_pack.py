#!/usr/bin/python2
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016   Tuxicoman

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
parser = argparse.ArgumentParser(description='Add extra data volume pack to Belgacom Internet')
parser.add_argument('login', type=str, help='Belgacom login email')
parser.add_argument('password', type=str, help='Belgacom password')
parser.add_argument('repeat', type=int, default=1, help='Nb volume packs to add (20GB each)')
args = parser.parse_args()

try:
  import selenium
except ImportError:
  print "Cannot import selenium. Try: $ pip install --user selenium=2.53"
  import sys
  sys.exit()
if selenium.__version__[0] != "2":
  print "Selenium version %s is used.\nSelenium 2.53 is preferred. Try: $ pip install --user selenium=2.53" % selenium.__version__

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By


headless = False
if headless == True:
  try:
    import pyvirtualdisplay
  except ImportError:
    print "Cannot import pyvirtualdisplay. Try: $ pip install --user pyvirtualdisplay"
    sys.exit()

  from pyvirtualdisplay import Display
  display = Display(visible=0, size=(1920, 1080))
  display.start()

browser = webdriver.Firefox()

def find_element_by_xpath_until_timeout(browser, xpath, timeout=10, clickable=False):
  element = WebDriverWait(browser, timeout).until(lambda browser:browser.find_element_by_xpath(xpath))
  if clickable == True:
    WebDriverWait(browser, timeout).until(expected_conditions.element_to_be_clickable((By.XPATH, xpath)))

  return element


print "Login ..."
browser.get('https://www.belgacom.be/login/fr/')
browser.switch_to_frame(browser.find_element_by_xpath('//iframe[@name="loginIframe"]'))
browser.switch_to_frame(browser.find_element_by_xpath('//iframe[@name="frame"]'))
browser.find_element_by_xpath('//input[@id="loginForm:userName"]').send_keys(args.login)
browser.find_element_by_xpath('//input[@id="loginForm:password"]').send_keys(args.password)
browser.find_element_by_xpath('//input[@id="loginForm:continue"]').click()
find_element_by_xpath_until_timeout(browser, '//div[@class="cm-header-tlc-title" and text()="Welcome to MyProximus"]')
print "Login done"


try:
  #Close the advertisement popup
  browser.find_element_by_xpath('//a[contains(@class, "oms-close-dialog")]').click()
except selenium.common.exceptions.NoSuchElementException:
  pass

find_element_by_xpath_until_timeout(browser,'//i[contains(@class, "icon-Internetlaptop")]', clickable=True).click()

for i in range(args.repeat):
  print "Round :", i+1
  print "Choosing Volume pack 20 free"
  find_element_by_xpath_until_timeout(browser,'//a[@href="#pb-tabs-notActivated"]', clickable=True).click()


  elements = browser.find_elements_by_xpath('//span[contains(@class,"og-unit")]')
  for element in elements :
    if "Extra Volume 20 GB" in element.get_attribute("innerHTML"):
      element.click()
      break

  find_element_by_xpath_until_timeout(browser,'//a[contains(@href,"myProducts/myOrder?selectedOption=hbs_volume_pack_20_free")]', clickable=True).click()
  find_element_by_xpath_until_timeout(browser,'//a[contains(@class,"pcp-order-next")]', clickable=True).click()

  print "Approving general terms"
  find_element_by_xpath_until_timeout(browser,'//input[@id="generalTerms"]', clickable=True).click()
  find_element_by_xpath_until_timeout(browser,'//a[@eventdetail="confirmOrderLink"]', clickable=True).click()

  print "Confirmation"
  find_element_by_xpath_until_timeout(browser,'//a[@href="/eservices/wps/myportal/myProducts"]', clickable=True).click()

#Show usage
print "Show current usage"
browser.get('https://admit.belgacom.be/eservices/wps/myportal/myBillAndUsage?clearSelectedProduct=true')

#browser.quit()

if headless == True:
  display.stop()

