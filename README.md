# Script to add Extra free volume packs to Belgacom Internet Illimité

Because Belgacom Internet Illimité is not illimité. After a few GB used, the bandwith is limited to 3Mbps and Belgacom starts to add ads into HTTP frames.

The trick they use to sell it as "illimité" is that you can add as many free extra volume packs of 20GB you want.

Of course, this is not fair because it's time consuming to do it manually and has no cost for them as you interact with their website.

So let's go and add them automatically by scripting.

# Requirement
You need :
- Python2
- selenium ($ pip install --user selenium=2.53)

# Example of use

$ python belgacom_add_volume_pack.py toto@titi.com password 10

The 2 first arguments are your belgacom login (email address + password)
The last argument is the number of extra free volume pack 20 GB you want to add.
